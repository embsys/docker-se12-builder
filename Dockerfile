FROM registry.forge.hefr.ch/embsys/arm-none-eabi-builder:latest
LABEL maintainer "Jacques Supcik <jacques.supcik@hefr.ch>"

# Get the latest libbbb from gitlab
RUN cd /tmp && \
    curl -L https://gitlab.forge.hefr.ch/embsys/libbbb/-/jobs/artifacts/master/download?job=build --output t.zip && \
    unzip t.zip && \
    cd build  && \
    tar zxvf libbbb-*-arm-none-eabi.tar.gz && \
    cp -lr /tmp/build/libbbb-*-arm-none-eabi/include/* ${TOOLCHAIN}/arm-none-eabi/include/ && \
    cp -lr /tmp/build/libbbb-*-arm-none-eabi/lib/* ${TOOLCHAIN}/arm-none-eabi/lib/

# Copy makefiles, headers and library to /se12/bbb for compatibility
COPY make.d /se12/bbb/make
RUN mkdir /se12/bbb/source && \
    cp -lr /tmp/build/libbbb-*-arm-none-eabi/include/* /se12/bbb/source && \
    cp -lr /tmp/build/libbbb-*-arm-none-eabi/lib/* /se12/bbb/source
ENV LMIBASE="/se12"

# cleanup
RUN rm -Rf /tmp/t.zip /tmp/build

WORKDIR /se12
